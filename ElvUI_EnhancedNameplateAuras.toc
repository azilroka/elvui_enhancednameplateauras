## Interface: 50400
## Title: |cff1784d1ElvUI |rEnhanced Nameplate Auras
## Author: Sortokk, Azilroka, pvtschlag
## Version: 1.2
## Notes: Enhanced Nameplates for ElvUI
## RequiredDeps: ElvUI

libs\load_libs.xml
load_nameplates.xml
