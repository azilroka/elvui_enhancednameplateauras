local E, L, V, P, G, _ = unpack(ElvUI); --Inport: Engine, Locales, PrivateDB, ProfileDB, GlobalDB, Localize Underscore
local NP = E:GetModule('NamePlates')
local ENP = E:GetModule('EnhancedNamePlates')
--[[
	This file handles functions for the Castbar and aura modules of nameplates.
]]

ENP.GUIDLockouts = {}
ENP.GUIDDR = {}
ENP.resetDRTime = 18 --Time it tacks for DR to reset.
ENP.Aura_List = {}
ENP.Aura_Spellid = {}
ENP.Aura_Expiration = {}
ENP.Aura_Stacks = {}
ENP.Aura_Caster = {}
ENP.Aura_Duration = {}
ENP.Aura_Texture = {}
ENP.Aura_Type = {}
ENP.Aura_Target = {}

local AURA_TYPE = {
	["Buff"] = 1,
	["Curse"] = 2,
	["Disease"] = 3,
	["Magic"] = 4,
	["Poison"] = 5,
	["Debuff"] = 6,
}

local band = bit.band
local ceil = math.ceil
local twipe = table.wipe

local RaidIconIndex = {
	"STAR",
	"CIRCLE",
	"DIAMOND",
	"TRIANGLE",
	"MOON",
	"SQUARE",
	"CROSS",
	"SKULL",
}

local function DefaultFilterFunction(aura) 
	if (aura.duration < 600) then
		return true
	end
end

function ENP:SetAuraInstance(guid, spellid, expiration, stacks, caster, duration, texture, auratype, auratarget, overrideDR)
	local filter = false
	local name = GetSpellInfo(spellid)

	if auratype == -1 then
		name = "School Lockout";
	end

	local visibility = E.global['nameplate']['spellListDefault']['visibility']
	if visibility == 1 or (visibility == 3 and caster == UnitGUID('player')) then
		filter = true;
	end

	local spellList = E.global['nameplate']['spellList']
	if spellList[name] then
		visibility = spellList[name]['visibility']
		if visibility == 1 or (visibility == 3 and caster == UnitGUID('player')) then
			filter = true;
		else
			filter = false;
		end
	end

	if filter ~= true then
		return;
	end

	if guid and spellid and texture then
		if GetPlayerInfoByGUID(guid) then
			local DRType = ENP.drSpells[spellid]

			if DRType and not overrideDR then
				local newDR = { diminish = 0.5, DRExpire = GetTime() + ENP.resetDRTime }

				if ENP.GUIDDR[guid] and ENP.GUIDDR[guid][DRType] then
					if ENP.GUIDDR[guid][DRType].DRExpire < GetTime() then
						ENP.GUIDDR[guid][DRType].DRExpire = GetTime() + ENP.resetDRTime
						ENP.GUIDDR[guid][DRType].diminish = 0.5
					elseif ENP.GUIDDR[guid][DRType].diminish >= 0.25 then
						ENP.GUIDDR[guid][DRType].DRExpire = GetTime() + ENP.resetDRTime
						duration = duration * ENP.GUIDDR[guid][DRType].diminish
						ENP.GUIDDR[guid][DRType].diminish = ENP.GUIDDR[guid][DRType].diminish / 2
					else
						-- This shouldn't happen
					end
				elseif ENP.GUIDDR[guid] and not ENP.GUIDDR[guid][DRType] then
					ENP.GUIDDR[guid][DRType] = newDR
				else
					ENP.GUIDDR[guid] = { }
					tinsert(ENP.GUIDDR[guid], { DRType = newDR })
				end
			end
		end

		--Special check for BG flags
		if spellid == 14267 or spellid == 14268 or spellid == 34976 then
			ENP:WipeAura(spellid)
		end

		local aura_id = spellid..(tostring(caster or "UNKNOWN_CASTER"))
		local aura_instance_id = guid..aura_id
		ENP.Aura_List[guid] = ENP.Aura_List[guid] or {}
		ENP.Aura_List[guid][aura_id] = aura_instance_id
		ENP.Aura_Spellid[aura_instance_id] = spellid
		ENP.Aura_Expiration[aura_instance_id] = expiration
		ENP.Aura_Stacks[aura_instance_id] = stacks
		ENP.Aura_Caster[aura_instance_id] = caster
		ENP.Aura_Duration[aura_instance_id] = duration
		ENP.Aura_Texture[aura_instance_id] = texture
		ENP.Aura_Type[aura_instance_id] = auratype
		ENP.Aura_Target[aura_instance_id] = auratarget
	end
end

function ENP:UpdateAuraByLookup(guid)
 	if guid == UnitGUID("target") then
		ENP:UpdateAurasByUnitID("target")
	elseif guid == UnitGUID("mouseover") then
		ENP:UpdateAurasByUnitID("mouseover")
	else
		ENP:UpdateAurasByGUID(guid)
	end
end

function ENP:COMBAT_LOG_EVENT_UNFILTERED(_, _, event, ...)
	local _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, spellid, spellName, _, auraType, stackCount, extraSchool = ...
	local isPvP = false

	if bit.band(destFlags, COMBATLOG_OBJECT_CONTROL_PLAYER) > 0 and bit.band(sourceFlags, COMBATLOG_OBJECT_CONTROL_PLAYER) > 0 then
		isPvP = true
	end

	local shortDestName = ENP:RemoveServerName(destName)
	local shortSourceName = ENP:RemoveServerName(sourceName)

	-- Cache Unit Name for alternative lookup strategy
	if shortDestName and destGUID and bit.band(destFlags, COMBATLOG_OBJECT_CONTROL_PLAYER) > 0 and not NP.ByName[shortDestName] then 
		NP.ByName[shortDestName] = destGUID
	end
	if shortSourceName and sourceGUID and bit.band(sourceFlags, COMBATLOG_OBJECT_CONTROL_PLAYER) > 0 and not NP.ByName[shortSourceName] then 
		NP.ByName[shortSourceName] = sourceGUID
	end

	if event == "SPELL_INTERRUPT" and ENP.lockouts[spellid] then
		local texture = GetSpellTexture(spellid)
		if not ENP.GUIDLockouts[destGUID] then
			ENP.GUIDLockouts[destGUID] = {}
		end
		ENP.GUIDLockouts[destGUID][extraSchool] = { dest = destGUID, source = sourceGUID, destSpell = auraType, sourceSpell = spellid, expire = GetTime() + ENP.lockouts[spellid], dur = ENP.lockouts[spellid], tex = texture }
		ENP:SetAuraInstance(destGUID, auraType, GetTime() + ENP.lockouts[spellid], 1, sourceGUID, ENP.lockouts[spellid], texture, -1, AURA_TARGET_HOSTILE, true)
	end

	if event == "SPELL_AURA_APPLIED" or event == "SPELL_AURA_REFRESH" then
		local duration = ENP:GetSpellDuration(spellid, isPvP)
		local expiration = 0
		local texture = GetSpellTexture(spellid)

		if duration > 0 then
			expiration = duration + GetTime()
		end

		if duration ~= -1 then
			ENP:SetAuraInstance(destGUID, spellid, expiration, 1, sourceGUID, duration, texture, AURA_TYPE_DEBUFF, AURA_TARGET_HOSTILE, false)
		end
	elseif event == "SPELL_AURA_APPLIED_DOSE" or event == "SPELL_AURA_REMOVED_DOSE" then
		local duration = ENP:GetSpellDuration(spellid, isPvP)
		local expiration = 0
		local texture = GetSpellTexture(spellid)
	elseif event == "SPELL_AURA_BROKEN" or event == "SPELL_AURA_BROKEN_SPELL" or event == "SPELL_AURA_REMOVED" then
		NP:RemoveAuraInstance(destGUID, spellid, sourceGUID)
	else
		if bit.band(sourceFlags, COMBATLOG_OBJECT_REACTION_HOSTILE) > 0 then 
			if bit.band(sourceFlags, COMBATLOG_OBJECT_CONTROL_PLAYER) > 0 then 
				--	destination plate, by name
				FoundPlate = NP:SearchNameplateByName(sourceName)
			elseif bit.band(sourceFlags, COMBATLOG_OBJECT_CONTROL_NPC) > 0 then 
				--	destination plate, by raid icon
				FoundPlate = NP:SearchNameplateByIconName(sourceRaidFlags) 
			else
				return
			end
		else
			return
		end
		
		if FoundPlate and FoundPlate:IsShown() and FoundPlate.unit ~= "target" then 
			FoundPlate.guid = sourceGUID
		end
	end

	if event == "SPELL_AURA_APPLIED" or event == "SPELL_AURA_REFRESH" or event == "SPELL_AURA_APPLIED_DOSE" or event == "SPELL_AURA_REMOVED_DOSE" or event == "SPELL_AURA_BROKEN" or event == "SPELL_AURA_BROKEN_SPELL" or event == "SPELL_AURA_REMOVED" then	
		ENP:UpdateAuraByLookup(destGUID, shortDestName)

		-- Cache Raid Icon Data for alternative lookup strategy
		for iconname, bitmask in pairs(NP.RaidTargetReference) do
			if bit.band(destRaidFlags, bitmask) > 0 then
				NP.ByRaidIcon[iconname] = destGUID
				raidicon = iconname
				break
			end
		end
	end	
	
end

function ENP:WipeAura(spellid)
	for guid, unit_aura_list in pairs(ENP.Aura_List) do 
		for aura_id, aura_instance_id in pairs(unit_aura_list) do
			if NP.Aura_Spellid[aura_instance_id] == spellid then
				NP.Aura_Spellid[aura_instance_id] = nil
				NP.Aura_Expiration[aura_instance_id] = nil
				NP.Aura_Stacks[aura_instance_id] = nil
				NP.Aura_Caster[aura_instance_id] = nil
				NP.Aura_Duration[aura_instance_id] = nil
				NP.Aura_Texture[aura_instance_id] = nil
				NP.Aura_Type[aura_instance_id] = nil
				NP.Aura_Target[aura_instance_id] = nil
				unit_aura_list[aura_id] = nil
			end
		end
	end
end

function ENP:GetSpellDuration(spellid, pvp)
	if ENP.auraInfoPvP[spellid] and pvp then
		return ENP.auraInfoPvP[spellid]
	elseif ENP.auraInfo[spellid] then
		return ENP.auraInfo[spellid]
	elseif NP.CachedAuraDurations[spellid] then
		return NP.CachedAuraDurations[spellid]
	else
		return -1
	end
end

function ENP:SetSpellDuration(spellid, duration)
	if spellid and not ENP.auraInfo[spellid] then 
		NP.CachedAuraDurations[spellid] = duration
	end
end

function ENP:UpdateIcon(frame, texture, expiration, stacks, duration, name)

	if frame and texture and name then
		local spell = E.global['nameplate']['spellList'][name]

		-- Icon
		frame.Icon:SetTexture(texture)

		-- Size
		local width = 20
		local height = 20

		if spell and spell['width'] then
			width = spell['width']
		elseif E.global['nameplate']['spellListDefault']['width'] then
			width = E.global['nameplate']['spellListDefault']['width']
		end

		if spell and spell['height'] then
			height = spell['height']
		elseif E.global['nameplate']['spellListDefault']['height'] then
			height = E.global['nameplate']['spellListDefault']['height']
		end

		if width > height then
			local aspect = height / width
			frame.Icon:SetTexCoord(0.07, 0.93, (0.5 - (aspect/2))+0.07, (0.5 + (aspect/2))-0.07)
		elseif height > width then
			local aspect = width / height
			frame.Icon:SetTexCoord((0.5 - (aspect/2))+0.07, (0.5 + (aspect/2))-0.07, 0.07, 0.93)
		else
			frame.Icon:SetTexCoord(0.07, 0.93, 0.07, 0.93)
		end

		frame:SetWidth(width)
		frame:SetHeight(height)

		-- Stacks
		local textSize = 7

		if spell and spell['text'] then
			textSize = spell['text']
		elseif E.global['nameplate']['spellListDefault']['text'] then
			textSize = E.global['nameplate']['spellListDefault']['text']
		end

		frame.Stacks:FontTemplate(nil, textSize, 'OUTLINE')
		if stacks > 1 then
			frame.Stacks:SetText(stacks)
		else
			frame.Stacks:SetText("")
		end

		if duration > 0 and expiration then
			frame.TimeLeft:FontTemplate(nil, textSize, 'OUTLINE')
			frame.TimeLeft:Show()

			-- Expiration
			NP:UpdateAuraTime(frame, expiration, duration)
 
			frame:Show()
			NP.PolledHideIn(frame, expiration, duration)
		else

			frame:Show()
			frame.TimeLeft:Hide()
			NP.PolledHideIn(frame, -1, duration)
		end
	else
		if UIFrameIsFlashing(frame) then
			UIFrameFlashStop(frame)
			frame:SetAlpha(1)
		end
		NP.PolledHideIn(frame, 0, duration)
	end
end

function ENP:UpdateIconGrid(parent, guid)
	local frame = NP.CreatedPlates[parent];
	local widget = frame.AuraWidget
	local AuraIconFrames = widget.AuraIconFrames
	local AurasOnUnit = NP:GetAuraList(guid)
	local AuraSlotIndex = 1
	local instanceid

	ENP.AurasCache = wipe(ENP.AurasCache)
	local auraCount = 0

	-- Cache displayable auras
	if AurasOnUnit then
		widget:Show()
		for instanceid in pairs(AurasOnUnit) do
			--for i,v in pairs(aura) do aura[i] = nil end
			local aura = {}
			aura.spellid, aura.expiration, aura.stacks, aura.caster, aura.duration, aura.texture, aura.type, aura.target = NP:GetAuraInstance(guid, instanceid)
			if tonumber(aura.spellid) then
				aura.name = GetSpellInfo(tonumber(aura.spellid))
				if aura.type == -1 then
					aura.name = "School Lockout"
				end
				aura.unit = frame.unit

				-- Get Order/Priority
				if aura.expiration > GetTime() or aura.duration == 0 then
					auraCount = auraCount + 1
					ENP.AurasCache[auraCount] = aura
				end
			end
		end
	end

	sort(ENP.AurasCache, 
		function(a, b)
			if E.db.nameplate['sortDirection'] == 0 then
				return a.expiration < b.expiration 
			else
				return a.expiration > b.expiration 
			end
		end
	)

	-- Display Auras
	local rightWidth = 0
	local leftWidth = 0
	if auraCount > 0 then 
		for index = 1, #ENP.AurasCache do
			local cachedaura = ENP.AurasCache[index]
			if cachedaura.spellid and cachedaura.expiration then 
				self:UpdateIcon(AuraIconFrames[AuraSlotIndex], cachedaura.texture, cachedaura.expiration, cachedaura.stacks, cachedaura.duration, cachedaura.name) 
				if index > 1 and mod(index,2) == 0 then
					rightWidth = rightWidth + AuraIconFrames[AuraSlotIndex]:GetWidth()
				elseif index > 1 and mod(index,2) ~= 0 then
					leftWidth = leftWidth + AuraIconFrames[AuraSlotIndex]:GetWidth()
				end
				AuraSlotIndex = AuraSlotIndex + 1
			end
			if AuraSlotIndex > NP.MAX_DISPLAYABLE_AURAS then break end
		end
	end

	if E.db.nameplate['auraAnchor'] and E.db.nameplate['auraAnchor'] == 2 then
		local offset = 0

		if rightWidth > leftWidth then
			offset = ( abs(rightWidth - leftWidth) / 2 ) * -1
		elseif leftWidth > rightWidth then
			offset = abs(leftWidth - rightWidth) / 2
		end

		AuraIconFrames[1]:SetPoint("BOTTOM", widget, offset, 3)
	end

	-- Clear Extra Slots
	for AuraSlotIndex = AuraSlotIndex, NP.MAX_DISPLAYABLE_AURAS do self:UpdateIcon(AuraIconFrames[AuraSlotIndex]) end

	ENP.AurasCache = wipe(ENP.AurasCache)
end

function ENP:UpdateAurasByUnitID(unit)
	local unitType
	if UnitIsFriend("player", unit) then unitType = AURA_TARGET_FRIENDLY else unitType = AURA_TARGET_HOSTILE end	
	--if unitType == AURA_TARGET_FRIENDLY then return end		-- Filter

	-- Check the units auras
	local index
	local guid = UnitGUID(unit)
	-- Reset Auras for a guid
	NP:WipeAuraList(guid)

	-- Debuffs
	for index = 1, 40 do
		local name , _, texture, count, dispelType, duration, expirationTime, unitCaster, _, _, spellid, _, isBossDebuff = UnitDebuff(unit, index)
		if not name then break end
		ENP:SetSpellDuration(spellid, duration)			-- Caches the aura data for times when the duration cannot be determined (ie. via combat log)
		ENP:SetAuraInstance(guid, spellid, expirationTime, count, UnitGUID(unitCaster or ""), duration, texture, AURA_TYPE[dispelType or "Debuff"], unitType, true)
	end

	-- Buffs
	for index = 1, 40 do
		local name , _, texture, count, dispelType, duration, expirationTime, unitCaster, _, _, spellid, _, isBossDebuff = UnitBuff(unit, index)
		if not name then break end
		ENP:SetSpellDuration(spellid, duration)			-- Caches the aura data for times when the duration cannot be determined (ie. via combat log)
		ENP:SetAuraInstance(guid, spellid, expirationTime, count, UnitGUID(unitCaster or ""), duration, texture, AURA_TYPE[dispelType or "Buff"], unitType, true)
	end

	if ENP.GUIDLockouts[guid] then
		for school,data in pairs(ENP.GUIDLockouts[guid]) do
			if data.expire > GetTime() then
				ENP:SetAuraInstance(data.dest, data.destSpell, data.expire, 1, data.source, data.dur, data.tex, -1, AURA_TARGET_HOSTILE, true)
			end
		end
	end

	local raidicon, name
	if UnitPlayerControlled(unit) then name = UnitName(unit) end
	raidicon = RaidIconIndex[GetRaidTargetIndex(unit) or ""]
	if raidicon then NP.ByRaidIcon[raidicon] = guid end

	local frame = NP:SearchForFrame(guid, raidicon, name)

	if frame then
		NP:UpdateAuras(frame)
	end
end

function ENP:UpdateAurasByGUID(guid)
	if not GetPlayerInfoByGUID(guid) then return end
	local name = UnitName(guid)
	local frame = NP:SearchForFrame(guid, nil, name)

	if frame then
		frame.guid = guid
		NP:UpdateAuras(frame)
	end
end

function ENP:UNIT_AURA(event, unit)
	if unit == "target" then
		self:UpdateAurasByUnitID("target")
	elseif unit == "focus" then
		self:UpdateAurasByUnitID("focus")
	elseif UnitIsPlayer(unit) then
		self:UpdateAurasByUnitID(unit)
	end
end

function ENP:RemoveServerName(name)
	if name ~= nil then
		local loc = name:find("-")
		if loc then
			name = name:sub(0, loc - 1)
		end
	end
	return name
end
